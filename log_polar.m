function out = log_polar(in)
%LOG_POLAR Summary of this function goes here
%   Detailed explanation goes here
 [nr, nc] = size(in);
 
 % Origin of the polar coordinate system
Xc = nc / 2;
Yc = nr / 2;

zr = nr /2;
 
out= zeros(nr, nc);

%base calculation //only map half of the row due to coefficient symmetry
 base = 10^(log(nr)/ zr);

%angle step size
 dTheta = (pi) / nc;
 
 for i = 1: nr
    %   Rows in the log-polar coordinate system represent the logarithm of
    %   the radial distance (RHO) from the origin.
    for j = 1: nc 
        %   Columns in the log-polar coordinate system represent the angle 
        %   (THETA) between the horizontal axis and the line through the
        %   origin.
        
        % Convert Cartesian coordinates to log-polar coordinates
        radius = base ^ (i - 1); 
        Theta = (j-1) * dTheta;
             
        % Convert log-polar coordinates to Cartesian coordinates
        x = radius * cos(Theta) + Xc;
        y = radius * sin(Theta) + Yc;
        
        if ((x < nc) &&(y < nr) && (x > 0) &&(y > 0))
            x00 = floor(x);
            x01 = x00;
            x10 = ceil(x);
            x11 = x10;
        
            y0 = floor(y);
            y1 = ceil(y);
        
            fx0 = (x10-x)/(x10-x00)*in(y00,x00) + (x-x00)/(x10-x00)*in(y10,x10);
            fx1 = (x11-x)/(x11-x01)*in(y01,x01) + (x-x01)/(x11-x01)*in(y11,x11);
        
            val = (y-y0)/(y1-y0)*fx1 + (y1-y)/(y1-y0)*fx0;
        
            out(i,j) = val;
        end
        
    end
 end

end

