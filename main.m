[f1, f2] = inputim();

imshow(f1);
pause();
imshow(f2);
pause();

%for two images f1 and f2
%DFT
F1 = fftshift(fft2(f1));
F2 = fftshift(fft2(f2));

%magnitude
F1m = abs(F1);
F2m = abs(F2);

imshow(F1m,[0,255]);
pause();
imshow(F2m,[0,255]);
pause();

%high pass filter
F1m_filtered = high_pass(F1m);
F2m_filtered = high_pass(F2m);

%log-polar transform
F1m_lp = log_polar(F1m_filtered);
F2m_lp = log_polar(F2m_filtered);

imshow(F1m_lp,[0,255]);
pause();
imshow(F2m_lp,[0,255]);
pause();

%phase correlation
[a, s] = phase_cor(F1m_lp, F2m_lp);

%image transformation
%scaling
%rotation by angle
%%[x1, y1, val1] = phase_cor(F1, F2_trans);
%rotation by angle + 180
%%[x, y, max] = phase_cor(F1, F2_trans);

%comparison
%%if (val1 > max)
%%    x = x1;
%%    y = y1;
%%    max = val1;
%%end
  