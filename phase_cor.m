function [x,y, val] = phase_cor(im1, im2)
%PHASE_COR Summary of this function goes here
%   Detailed explanation goes here

    [nr, nc] = size(im1);
    fim1 = fft2(im1);
    fim2 = fft2(im2);
    
    %ifft of the phase angle only
    CC = ifft2(angle(fim1.*conj(fim2)));
    
    %find the maximum peak
    [max1,loc1] = max(CC);
    [max2,loc2] = max(max1);
    
    x = loc2;
    y = loc1(loc2);
    
    val = max2;

end

