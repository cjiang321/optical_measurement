function res = high_pass(in)
%HIGH_PASS Summary of this function goes here
%   Detailed explanation goes here
    [nr, nc]= size(in);
    
    %step size
    dnr = 1 / (nr - 1);
    dnc = 1 / (nc - 1);

    Eta = cos(pi * (-0.5 : dnr : 0.5));
    Neta = cos(pi * (-0.5 : dnc : 0.5));
    X = Eta' * Neta;
   
    % Output of the transfer function
    H = (1.0 - X).*(2.0 - X);
    
    res = in .* H;
end

