function [f1, f2] = inputim()
%INPUT Summary of this function goes here
%   Detailed explanation goes here

    im1 = imread('p2.jpg');
    im2 = imread('p3.jpg');
    
    im1 = rgb2gray(im1);
    im2 = rgb2gray(im2);
    
    f1 = im1(1:256,1:256);
    f2 = im2(1:256,1:256);
    
end

