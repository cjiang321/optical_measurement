%   Developer:      Gajendren Kasithasan
%   Supervisors:    Dr. D. Armitage & Professor A. Peyton
%   The University of Manchester





function H = Hamming2D(image)
%HAMMING2D filters an image using a two-dimensional Hamming window.

%   F = HAMMING2D(IMAGE) creates a two-dimensional Hamming window based on
%   the following equation:
%   H(i, j) = [0.54 - 0.46*cos((2*PI*i)/N)][0.54 - 0.46*cos((2*PI*j)/N)]
%   In the above equation, 'N' is the window length. One particular use of 
%   this window is to reduce edge effects of an image.  

% % clear;
% % tic;

%%                         -- TEST IMAGE 1 -- 

% Read image
% % image = rgb2gray(imread('Lion Statue.jpg'));

% Display input
% % figure; imshow(image); title(sprintf(['INPUT IMAGE\nSIZE ' ...
% %     num2str(size(image))]));





%%                     -- Apply Hamming Window --

% Calculate the number of rows and columns in the image
[rows, columns] = size(image);

% Create one-dimensional Hamming Windows along horizontal and vertical
% directions
for j = 1:columns
    HammingWindowColumns(:,j) = (0.54 - 0.46*cos((2*pi*j)/columns));
end
for i = 1:rows
    HammingWindowRows(i,:) = (0.54 - 0.46*cos((2*pi*i)/rows));
end

% Initalise space for mask
mask = zeros(rows, columns);

% Initalise windows along row and column directions of mask
for j = 1:columns
  maskRows(:, j) = HammingWindowRows;
end
for i = 1:rows
  maskColumns(i, :) = HammingWindowColumns;
end
mask = maskRows .* maskColumns;

% Apply window on image
H = im2double(image) .* mask;

% Display image
% % figure; imshow(f); title('FILTERED IMAGE');

% % toc;