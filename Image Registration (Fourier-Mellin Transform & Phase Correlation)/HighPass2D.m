%   Developer:      Gajendren Kasithasan
%   Supervisors:    Dr. D. Armitage & Professor A. Peyton
%   The University of Manchester





function H = HighPass2D(height, width)
%HIGH-PASS-2D filters an image using a two-dimensional High-Pass filter.

%   F = HIGHPASS2D(IMAGE) creates a two-dimensional High-Pass filter 
%   based on the following equation:
%   H(i, j) = [1.0 - X(i, j)]*[2.0 - X(i, j)]
%   where X(i, j) = [cos(PI * i) * cos(PI * j)] and i >= -0.5, j <= 0.5  

% % clear;
% % tic;

%%                           -- TEST IMAGE 1 -- 

% Read image
% % image = rgb2gray(imread('Lion Statue.jpg'));

% Display input
% % figure; imshow(image); title(sprintf(['INPUT IMAGE\nSIZE ' ...
% %     num2str(size(image))]));





%%                     -- Apply High Pass Filter --

% Step size for height and width
dHeight = 1 / (height - 1);
dWidth = 1 / (width - 1);

% Matrix design for the high-pass filter
Eta = cos(pi * (-0.5 : dHeight : 0.5));
Neta = cos(pi * (-0.5 : dWidth : 0.5));
X = Eta' * Neta;

% Output of the transfer function
H = (1.0 - X).*(2.0 - X);

% Display filter matrix
% % figure; imshow(H); title(sprintf(['HIGH-PASS FILTER MATRIX\nSIZE ' ...
% %     num2str(size(H))]));

% % toc