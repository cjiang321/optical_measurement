%   Developer:      Gajendren Kasithasan
%   Supervisors:    Dr. D. Armitage & Professor A. Peyton
%   The University of Manchester





function f = Zero_Padding(image)
%ZERO-PADDING converts any N x M  grayscale image into a P x P  grayscale 
%image by padding with zeros. 

%   F = ZERO_PADDING(IMAGE) performs zero-padding on the desired grayscale 
%   image, which in this case is known as 'IMAGE', to give it equal length 
%   and width dimensions. This is quite useful when computing the Hadamard
%   product of two matrices of different dimensions.

% % clear;
% % tic;

%%                           -- TEST IMAGE 1 -- 

% Read image
% % image = rgb2gray(imread('Lion Statue.jpg'));

% Display input
% % figure; imshow(image); title(sprintf(['INPUT IMAGE\nSIZE ' ...
% %     num2str(size(image))]));





%%                           -- IMAGE SIZE -- 

% Calculate the number of rows and columns in the image
[rows, columns] = size(image);





%%                           -- ZERO-PADDING --

% Zero-pad the image
if rows == columns
    f = image;
elseif rows > columns
    leftMatrix = zeros(rows, round((rows - columns)/2));
    rightMatrix = zeros(rows, round((rows - columns - 1)/2));
    f = [leftMatrix, image, rightMatrix];
else
    topMatrix = zeros(round((columns - rows)/2), columns);
    bottomMatrix = zeros(round((columns - rows - 1)/2), columns);
    f = [topMatrix; image; bottomMatrix];
end

% Display output
% % figure; imshow(f); title(sprintf(['OUTPUT IMAGE\nSIZE ' ...
% %     num2str(size(f))]));

% % toc
