%   Developer:      Gajendren Kasithasan
%   Supervisors:    Dr. D. Armitage & Professor A. Peyton
%   The University of Manchester





function [RowShift, ColumnShift] = SPOMF(image1, image2)
%SPOMF (Symmetric Phase-Only Matched Filter) calculates the translation
%parameters of two images (one being thetranslated version of the other).

%   F = SPOMF(IMAGE1, IMAGE2) performs Discrete Fourier Transform on the 
%   two images, which in this case are known as 'IMAGE1' and 'IMAGE2', and 
%   finds the translation parameter through phase correlation. It should be 
%   noted that the two images are related through the translation
%   parameter, which specifies how much one image is translated with
%   respect to the other. 

%   This function only recovers the translation values for a displaced
%   image and not for the components of an image.

% % clear;
% % tic;

%%                         -- EXAMPLE IMAGES -- 

% Read images
% % image1 = imread('Lion Statue.jpg');
% % image2 = imread('Ellipse 2.jpg');

% Display image
% % figure; imshow(image1); title(sprintf(['INPUT IMAGE 1\nSIZE ' ...
% %     num2str(size(image1))]));
% % figure; imshow(image2); title(sprintf(['INPUT IMAGE 2\nSIZE ' ...
% %     num2str(size(image2))]));





%%  -- Converting Colour Images to a Grayscale Images and pre-process --

% Convert to Grayscale
% % image1 = rgb2gray(image1);
% % image2 = rgb2gray(image2);

% Display Image
% % figure; imshow(image1); title(sprintf(['GRAYSCALE IMAGE 1\nSIZE ' ...
% %     num2str(size(image1))]));
% % figure; imshow(image2); title(sprintf(['GRAYSCALE IMAGE 2\nSIZE ' ...
% %     num2str(size(image2))]));

% Equalise length and width
% % image1 = Zero_Padding(image1);
% % image2 = Zero_Padding(image2);

% Display image
% % figure; imshow(image1); title(sprintf(['ZERO-PADDED IMAGE 1\nSIZE ' ...
% %     num2str(size(image1))]));
% % figure; imshow(image2); title(sprintf(['ZERO-PADDED IMAGE 2\nSIZE ' ...
% %     num2str(size(image2))]));





%%                    -- Translate Test Image --

% % % Translated image
% % image2 = image1;
% % 
% % % Calculate the number of rows and columns in the image
% % [rows, columns] = size(image2);
% % 
% % % Integer translation along the horizontal direction
% % tx = 0;
% % 
% % % Integer translation along the vertical direction
% % ty = 200;   
% % 
% % % Translate in the horizontal direction
% % if tx == 0
% %     ;    
% % elseif tx > 0
% %     for i = 1:abs(tx)
% %         image2(:, end) = [];
% %     end
% %     leftMatrix = zeros(rows, abs(tx));
% %     image2 = [leftMatrix, image2];
% % else
% %     for i = 1:abs(tx)
% %         image2(:, 1) = [];
% %     end
% %     rightMatrix = zeros(rows, abs(tx));
% %     image2 = [image2, rightMatrix];
% % end
% % %   A positive value translates the image towards the right, and vice
% % %   versa.
% % 
% % % Translate in the vertical direction 
% % if ty == 0
% %     ;    
% % elseif ty > 0
% %     for j = 1:abs(ty)
% %         image2(end, :) = [];
% %         topMatrix = zeros(abs(ty), columns);
% %     end
% %     topMatrix = zeros(abs(ty), columns);
% %     image2 = [topMatrix; image2];
% % else
% %     for j = 1:abs(ty)
% %         image2(1, :) = [];
% %     end
% %     bottomMatrix = zeros(abs(ty), columns);
% %     image2 = [image2; bottomMatrix];
% % end
% % %   A positive value translates the image towards the bottom, and vice
% % %   versa.
% % 
% % % Display image
% % figure; imshow(image2); title(sprintf(['TRANSLATED IMAGE\nSIZE ' ...
% %     num2str(size(image2))]));





%%                        -- HAMMING WINDOW --

% Apply a two-dimensional Hamming window on IMAGE 1
image1 = Hamming2D(image1);

% Dispaly image
% % figure; imshow(image1); title(sprintf(['IMAGE 1\nSIZE ' ...
% %     num2str(size(image1))]));

% Apply a two dimensional Hamming window on IMAGE 2
image2 = Hamming2D(image2);

% Display image
% % figure; imshow(image2); title(sprintf(['IMAGE 2\nSIZE ' ...
% %     num2str(size(image2))]));





%%                    -- Discrete Fourier Transform --

% 2-D Fast Fourier Transform
FourierTransform1 = fft2(double(image1));
FourierTransform2 = fft2(double(image2));

% Display images
% % figure; imshow(abs(fftshift(log(1 + FourierTransform1))), []); ...
% %     title(sprintf(['SPECTRAL DOMAIN (IMAGE1) - MAGNITUDE\nSIZE ' ...
% %     num2str(size(FourierTransform1))]));
% % figure; imshow(angle(fftshift(FourierTransform1)),[-pi pi]); ...
% %     title(sprintf(['SPECTRAL DOMAIN (IMAGE1) - PHASE\nSIZE ' ...
% %     num2str(size(FourierTransform1))]));
% % figure; imshow(abs(fftshift(log(1 + FourierTransform2))), []); ...
% %     title(sprintf(['SPECTRAL DOMAIN (IMAGE2) - MAGNITUDE\nSIZE ' ...
% %     num2str(size(FourierTransform2))]));
% % figure; imshow(angle(fftshift(FourierTransform2)),[-pi pi]); ...
% %     title(sprintf(['SPECTRAL DOMAIN (IMAGE2) - PHASE\nSIZE ' ...
% %     num2str(size(FourierTransform2))]));





%%                       -- PHASE CORRELATION --

% Cross-power spectrum
CrossPowerSpectrum = (FourierTransform1 .* conj(FourierTransform2)) ./ ...
    abs(FourierTransform1 .* conj(FourierTransform2));





%%                   -- Inverse Fourier Transform --

% Inverse Fourier Transform
PeakCorrelationMatrix = abs(ifft2(CrossPowerSpectrum));





%%         -- Locate Peak Value & Compute Displacement Vector --

% Display image and plot
% % figure; imshow(PeakCorrelationMatrix); ...
% %     title(sprintf(['INVERSE OF CROSS-POWER SPECTRUM\nSIZE '...
% %     num2str(size(PeakCorrelationMatrix))]));
figure; mesh(PeakCorrelationMatrix); ...
    title(sprintf(['INVERSE OF CROSS-POWER SPECTRUM\nSIZE '...
    num2str(size(PeakCorrelationMatrix))]));

% Locate peak value
[value, location] = max(PeakCorrelationMatrix(:));
[RowLocation,ColumnLocation] = ind2sub(size(PeakCorrelationMatrix), location);

% Conditioning translation values
if RowLocation > (size(PeakCorrelationMatrix, 1) / 2) 
    RowShift = size(PeakCorrelationMatrix,1) - (RowLocation - 1);
    %    Moving towards the bottom of an image is considered to be a
    %    positive displacement.
else
    RowShift = -(RowLocation - 1);
end

if ColumnLocation > (size(PeakCorrelationMatrix, 2) / 2)
    ColumnShift = ColumnLocation - 1 - size(PeakCorrelationMatrix, 2);
    %   Moving towards the right of an image is considered to be a positive
    %   displacement.
else
    ColumnShift = ColumnLocation - 1;
end

value
RowLocation
ColumnLocation
RowShift
ColumnShift

% % toc;
