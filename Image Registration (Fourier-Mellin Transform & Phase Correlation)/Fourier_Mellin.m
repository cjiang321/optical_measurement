%   Developer:      Gajendren Kasithasan
%   Supervisors:    Dr. D. Armitage & Professor A. Peyton
%   The University of Manchester





function [ScaledValue, RotatedValue] = Fourier_Mellin(image1, image2)
%FOURIER-MELLIN figures out the scaling and rotation parameters of the
%image.

%   F = FOURIER_MELLIN(IMAGE1, IMAGE2)performs Fourier-Mellin Transform on 
%   the image, which in this case are known as 'IMAGE1' and 'IMAGE2',
%   and computes the scaling and rotation values through phase-correlation.
%   IMAGE1 is the template image for the image registration process, while 
%   IMAGE2 is the image which will be registered.

clear;
tic;

%%                         -- TEST IMAGES -- 

% Read image
image1 = imread('Calculator 27cm.jpg');
image2 = imread('Calculator 24.5cm.jpg');

% Display image
% % figure; imshow(image1); title(sprintf(['INPUT IMAGE 1\nSIZE ' ...
% %     num2str(size(image1))]));
% % figure; imshow(image2); title(sprintf(['INPUT IMAGE 2\nSIZE ' ...
% %     num2str(size(image2))]));





%%                        -- PRE-PROCESSING --

% Convert to Grayscale
% % image1 = rgb2gray(image1);
% % image2 = rgb2gray(image2);

% Display image
figure; imshow(image1); title(sprintf(['GRAYSCALE IMAGE 1\nSIZE ' ...
    num2str(size(image1))]));
% % figure; imshow(image2); title(sprintf(['GRAYSCALE IMAGE 2\nSIZE ' ...
% %     num2str(size(image2))]));

% Equalise length and width
image1 = Zero_Padding(image1);
image2 = Zero_Padding(image2);

% Reduce Edge Effects
image1 = Hamming2D(image1);
image2 = Hamming2D(image2);

% Image Size
[rows, columns] = size(image1);

% Display image
% % figure; imshow(image1); title(sprintf(['ZERO-PADDED IMAGE 1\nSIZE ' ...
% %     num2str(size(image1))]));
% % figure; imshow(image2); title(sprintf(['ZERO-PADDED IMAGE 2\nSIZE ' ...
% %     num2str(size(image2))]));





%%                 -- Scaling & Rotation & Translation --

% % % Parameters
% % Scaling = 2.2;
% % Rotation = 0;
% % 
% % % Zoom-in Location
% % rowLocation = 0.5;
% % columnLocation = 0.5;
% % %   In order to zoom-in to the centre of the image, ROW-LOCATION should be 
% % %   inatilised to a value of 0.5 and COLUMN-LOCATION should be inatilised 
% % %   to a value of 0.5. 
% % 
% % % Image to modify
% % image2 = image1;
% % 
% % % Scaling
% % imageScaled = imresize(image2, Scaling);
% % 
% % % Cropping
% % if (size(imageScaled, 1) > size(image1, 1)) || ...
% %         (size(imageScaled, 2) > size(image1, 2))  
% %     imageCropped = imageScaled(...
% %         (rowLocation * (size(imageScaled, 1)) - ((rowLocation * rows) - 1)): ...
% %         (rowLocation * (size(imageScaled, 1)) + ((1 - rowLocation) * rows)), ...
% %         (columnLocation * (size(imageScaled, 2)) - ((columnLocation * columns) - 1)): ...
% %         (columnLocation * (size(imageScaled, 2)) + ((1 - columnLocation) * columns)));
% % else
% %     leftMatrix = zeros(size(imageScaled, 1), round((columns - size(imageScaled, 2))/2));
% %     rightMatrix = zeros(size(imageScaled, 1), round((columns - size(imageScaled, 2) - 1)/2));
% %     imageCropped = [leftMatrix, imageScaled, rightMatrix];
% %     topMatrix = zeros(round((rows - size(imageScaled, 1))/2), size(imageCropped, 2));
% %     bottomMatrix = zeros(round((rows - size(imageScaled, 1) - 1)/2), size(imageCropped, 2));
% %     imageCropped = [topMatrix; imageCropped; bottomMatrix];
% % end
% % 
% % % Rotating
% % imageRotated = imrotate(imageCropped, Rotation, 'bilinear', 'crop');
% % 
% % % Equalise length and width
% % image2 = Zero_Padding(imageRotated);
% % 
% % % Display modified image
% % figure; imshow(image2); title(sprintf(['ZERO-PADDED IMAGE 2\nSIZE ' ...
% %     num2str(size(image2))]));





%%                    -- Discrete Fourier Transform --

% 2-D Fast Fourier Transform
FourierTransform1 = abs(fftshift(fft2(image1)));
FourierTransform2 = abs(fftshift(fft2(image2)));

% Display images
figure; imshow(abs((log(1 + FourierTransform1))), []); ...
    title(sprintf(['SPECTRAL DOMAIN (IMAGE1) - MAGNITUDE\nSIZE ' ...
    num2str(size(FourierTransform1))]));
figure; imshow(abs((log(1 + FourierTransform2))), []); ...
    title(sprintf(['SPECTRAL DOMAIN (IMAGE2) - MAGNITUDE\nSIZE ' ...
    num2str(size(FourierTransform2))]));
%   The dynamic range of the Fourier coefficients (i.e. the intensity 
%   values in the Fourier image) is too large to be displayed on the 
%   screen, therefore all other values appear as black. Therefore we apply
%   a logarithmic transformation. A value of 1 is added to the transformed 
%   image before taking the logarithm because the logarithm is not defined
%   for values of 0.





%%                       -- HIGH-PASS FILTER --

% Convolve with a high-pass filter
FourierTransform1 = HighPass2D(size(image1, 1), size(image1, 2)) .* ...
    FourierTransform1;
FourierTransform2 = HighPass2D(size(image2, 1), size(image2, 2)) .* ...
    FourierTransform2;

% Display images
figure; imshow((log(1 + FourierTransform1)), []); ...
    title(sprintf(['SPECTRAL DOMAIN (IMAGE1) - MAGNITUDE\nSIZE ' ...
    num2str(size(FourierTransform1))]));
figure; imshow((log(1 + FourierTransform2)), []); ...
    title(sprintf(['SPECTRAL DOMAIN (IMAGE2) - MAGNITUDE\nSIZE ' ...
    num2str(size(FourierTransform2))]));





%%                     -- Log-Polar Conversion --

%   The assumption made here is that the horizontal axis (X and RHO) starts
%   from the upper-left corner and ends at the upper-right corner, and the 
%   vertical axis (Y and THETA) begins at the upper-left corner and ends at 
%   the lower-left corner.

% Transform from Cartesian space to log-polar space
[LogPolarTransform1, base1] = LPT(FourierTransform1);
[LogPolarTransform2, base2] = LPT(FourierTransform2);

% Display images
figure; imshow(LogPolarTransform1, [0 255]); ...
    title(sprintf(['LOG-POLAR CONVERSION 1\nSIZE '... 
    num2str(size(LogPolarTransform1))]));
figure; imshow(LogPolarTransform2, [0 255]); ...
    title(sprintf(['LOG-POLAR CONVERSION 2\nSIZE '... 
    num2str(size(LogPolarTransform2))]));





%%                      -- Phase-Correlation --

% Compute phase-correlation
[RowShift, ColumnShift] = SPOMF(LogPolarTransform1, LogPolarTransform2);





%%               -- Derived Scaling & Rotation Values --           

% Computed scaling value
ScaledValue = 1 / (base1 ^ RowShift)

% Computed rotational value
RotatedValue = (360 / columns) * ColumnShift





% % toc
end




function [LogPolarTransform, base] = LPT(image)
%LPT converts any Cartesian coordinate system into a log-polar coordinate 
%system. 

%   F = LPT(IMAGE) converts the desired grayscale image, which in this case
%   is known as 'IMAGE', from a Cartesian coordinate System into a 
%   log-polar coordinate system. 
%
%   The assumption made here is that the horizontal axis (X and RHO) starts
%   from the upper-left corner and ends at the upper-right corner, and the 
%   vertical axis (Y and THETA) begins at the upper-left corner and ends at 
%   the lower-left corner.

% % clear;
% % tic;

%%                      -- Log-Polar Conversion --

% Calculate the number of rows and columns in the image 
[rows, columns] = size(image);

% Origin of the polar coordinate system
Xc = columns / 2;
Yc = rows / 2;

% Step size for THETA
dTheta = (2*pi) / columns;
%   The THETA value wraps around 2*PI. 

% Logarithmic Conversion
base = 10 ^ (log10(rows) / rows); 
%   This base value will allow the the log-polar coordinate system to have
%   the same number of rows as the Cartesian coordinate system.

for i = 1:rows 
    %   Rows in the log-polar coordinate system represent the logarithm of
    %   the radial distance (RHO) from the origin.
    for j = 1:columns 
        %   Columns in the log-polar coordinate system represent the angle 
        %   (THETA) between the horizontal axis and the line through the
        %   origin.
        
        % Convert Cartesian coordinates to log-polar coordinates
        radius = base ^ (i - 1); 
        Theta = j * dTheta;
             
        % Convert log-polar coordinates to Cartesian coordinates
        x = round(radius * cos(Theta) + Xc);
        y = round(radius * sin(Theta) + Yc);
        %   The X and Y coordinates will allow for bilinear interpolation
        %   from the Cartesian coodinate system to the log-polar coordinate
        %   system.
        
        % Interpolation
        if (x > 0) && (y > 0) && (x < size(image,2)) && (y < size(image,1)) 
            output(i,j) = image(y,x);            
        end
        
    end
end

% Return output
LogPolarTransform = output;

% % toc
end