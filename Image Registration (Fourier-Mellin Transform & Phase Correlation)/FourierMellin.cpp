#define _USE_MATH_DEFINES

#include <opencv2\opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace cv;
using namespace std;

double round (double number);
Mat ZeroPadding (Mat image, int rows, int columns);
Mat Hamming2D (Mat image);
Mat DFT (Mat image);
Mat HighPass2D (Mat image);
Mat LogPolar (Mat image);





int main( int argc, char** argv )
{
    // READ IMAGE 

	//char filename[]= "G:\\EEEN40035 Fourth Year Project\\Demos\\Image Registration (Fourier-Mellin Transform & Phase Correlation)\\Lion Statue.jpg";
	//cv::Mat image1 = imread(filename, 0); // Read from file and convert to grayscale
    ////namedWindow( "GRAYSCALE IMAGE", WINDOW_AUTOSIZE ); // Create a window for display.
    ////imshow( "GRAYSCALE IMAGE", image ); // Show image within the window.
	//cout << "Image file acquired successfully. \nIMAGE 1 SIZE: " << image1.rows << "x" << image1.cols << endl << endl;

	VideoCapture cap(0); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;

	Mat image1;
    cap >> image1; // get a new frame from camera
    cvtColor(image1, image1, CV_BGR2GRAY);
	namedWindow( "CAMERA", WINDOW_AUTOSIZE);
	imshow( "CAMERA", image1);




	// EQUALISE LENGTH & WIDTH

	image1 = ZeroPadding (image1, image1.rows, image1.cols);

	//namedWindow( "ZERO-PADDED IMAGE", WINDOW_AUTOSIZE ); // Create a window for display.
    //imshow( "ZERO-PADDED IMAGE", image1 ); // Show image within the window.
	cout << "Height and width of image file successfully equalised. \nIMAGE 1 SIZE: " << image1.rows << "x" << image1.cols << endl << endl;

	Size size = image1.size();
	int Width = size.width, Height = size.height;



	
	// SCALING & ROTATION

	//double Scaling = 2.01;	

	//cv::Mat image2;
	//resize(image1, image2, Size(), Scaling, Scaling, INTER_LINEAR);
	//
	//if (Scaling == 1)
	//{
	//	image2 = image1.clone();
	//}
	//else if (Scaling > 1)
	//{
	//	Point2i center(image2.cols/2, image2.rows/2);
	//	getRectSubPix(image2, image1.size(), center, image2, -1);
	//}
	//else 
	//{
	//	image2 = ZeroPadding (image2, image1.rows, image2.cols);
	//	image2 = ZeroPadding (image2, image2.rows, image2.cols);
	//}
	//
	////namedWindow( "SCALED IMAGE", WINDOW_AUTOSIZE );
	////imshow( "SCALED IMAGE", image2);
	//cout << "A scaled version of the original image has been successfully created. \nIMAGE 2 SIZE: " << image2.rows << "x" << image2.cols << endl << endl;





	// HAMMING FILTER
	
	image1 = Hamming2D (image1);
	//image2 = Hamming2D (image2);

	//namedWindow( "HAMMING FILTER", WINDOW_AUTOSIZE ); // Create a window for display.
	//imshow( "HAMMING FILTER 1", image1/255 ); // Show image within the window.
	//imshow( "HAMMING FILTER 2", image2/255 );
	//cout << "HAMMING FILTER successfully applied on both image files. \nIMAGE 1 SIZE: " << image1.rows << "x" << image1.cols << "\tIMAGE 2 SIZE: " << image2.rows << "x" << image2.cols << endl << endl;





	// FOURIER TRANSFORM

	cv::Mat magI1 = DFT (image1);
	//cv::Mat magI2 = DFT (image2);

	normalize(magI1, magI1, 0, 1, CV_MINMAX); // Transform the matrix with float values into a viewable image form (float between values 0 and 1).
	//normalize(magI2, magI2, 0, 1, CV_MINMAX);

	//imshow( "FOURIER SPECTRUM MAGNITUDE 1", magI1);
    //imshow( "FOURIER SPECTRUM MAGNITUDE 2", magI2);
    //cout << "Image file successfully converted to Fourier domain. Fourier magnitude has been computed and image origin shifted to image centre. \nIMAGE 1 SIZE: " << magI1.rows << "x" << magI1.cols << "\tIMAGE 2 SIZE: " << magI2.rows << "x" << magI2.cols << endl << endl;




	// HIGH-PASS FILTER
		
	magI1 = HighPass2D (magI1);
	//magI2 = HighPass2D (magI2);

	//imshow( "HIGH-PASS FILTER 1", magI1);
	//imshow( "HIGH-PASS FILTER 2", magI2);
	//cout << "HIGH-PASS FILTER successfully applied.\nIMAGE 1 SIZE: " << magI1.rows << "x" << magI1.cols << "\tIMAGE 2 SIZE: " << magI2.rows << "x" << magI2.cols << endl << endl;





	// LOG-POLAR CONVERSION
	
	cv::Mat LogPolarI1 = LogPolar (magI1);
	//cv::Mat LogPolarI2 = LogPolar (magI2);

	//imshow( "LOG-POLAR TRANSFORM 1", LogPolarI1);
	//imshow( "LOG-POLAR TRANSFORM 2", LogPolarI2);
	//cout << "Fourier data successfully converted to Log-Polar domain. \nIMAGE 1 SIZE: " << LogPolarI1.rows << "x" << LogPolarI1.cols << "\tIMAGE 2 SIZE: " << LogPolarI2.rows << "x" << LogPolarI2.cols << endl << endl;





	// PHASE CORRELATION

	cv::Mat curr, prev, curr64f, prev64f, hann;
	
	curr = LogPolarI1.clone();
	//prev = LogPolarI2.clone();

	if(prev.empty())
    {
        prev = curr.clone();
        //createHanningWindow(hann, curr.size(), CV_64F);
    }

	createHanningWindow(hann, curr.size(), CV_64F);

    prev.convertTo(prev64f, CV_64F);
    curr.convertTo(curr64f, CV_64F);

	Point2d shift = phaseCorrelate(prev64f, curr64f, hann);
    double radius = cv::sqrt(shift.x*shift.x + shift.y*shift.y);

    //imshow("phase shift", frame2);
    cout << "SHIFT: " << shift << endl << endl;    
	
	double base = pow(10.0, ((log10((double)image1.rows))/image1.rows));
	cout << "SCALING: " << 1/pow(base, shift.y) << endl;

	prev = curr.clone();



    waitKey(0); // Wait for a keystroke in the window
    return 0;
}





double round(double number)
{
  return floor(number + 0.5);
}





Mat ZeroPadding (Mat image, int rows, int columns)
{
	int leftMatrix = 0; int rightMatrix = 0; int topMatrix = 0; int bottomMatrix = 0;
	
	if (rows == columns)
	{
		image = image.clone();
	}
	else if (rows > columns) 
	{
		leftMatrix = (int) ceil((rows - columns) / 2.0);
		rightMatrix = (int) ceil((rows - columns - 1) / 2.0);
	}
	else 
	{
		topMatrix = (int) ceil((columns - rows) / 2.0);
		bottomMatrix = (int) ceil((columns - rows - 1) / 2.0);
	}

	copyMakeBorder(image, image, topMatrix, bottomMatrix, leftMatrix, rightMatrix, BORDER_CONSTANT, 0);
	
	return image;
}





Mat Hamming2D (Mat image)
{
	cv::Mat HammingFilter = cv::Mat::zeros(image.rows, image.cols, CV_32FC1);
	cv::Mat HammingFilterRow = cv::Mat::zeros(image.rows, 1, CV_32FC1); 
	cv::Mat HammingFilterColumn = cv::Mat::zeros(1, image.cols, CV_32FC1);
	cv::Mat maskRow = cv::Mat::zeros(image.rows, image.cols, CV_32FC1); 
	cv::Mat maskColumn = cv::Mat::zeros(image.rows, image.cols, CV_32FC1); 
	
	for (int i = 0; i < HammingFilterRow.rows; i++)
	{
		HammingFilterRow.row(i) = (0.54 - 0.46*cos((2*M_PI*i)/HammingFilterRow.rows));
	}
	
	for (int i = 0; i < HammingFilterColumn.cols; i++)
	{
		HammingFilterColumn.col(i) = (0.54 - 0.46*cos((2*M_PI*i)/HammingFilterColumn.cols));
	}
	
	for (int i = 0; i < HammingFilter.rows; i++)
	{
		HammingFilterColumn.copyTo(maskRow.row(i));
	}
	
	for (int i = 0; i < HammingFilter.cols; i++)
	{
		HammingFilterRow.copyTo(maskColumn.col(i));
	}
	
	HammingFilter = maskRow.mul(maskColumn);
	image.convertTo(image, CV_32FC1);
	image = image.mul(HammingFilter);

	return image;
}




Mat DFT (Mat image)
{
	Mat padded;                            //expand input image to optimal size
    int m = getOptimalDFTSize( image.rows );
    int n = getOptimalDFTSize( image.cols ); // on the border add zero values
    copyMakeBorder(image, padded, 0, m - image.rows, 0, n - image.cols, BORDER_CONSTANT, Scalar::all(0));

    Mat planes[] = {Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F)};
    Mat complexI;
    merge(planes, 2, complexI);         // Add to the expanded another plane with zeros

    dft(complexI, complexI);            // this way the result may fit in the source matrix

    // compute the magnitude and switch to logarithmic scale
    // => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
    split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
    magnitude(planes[0], planes[1], planes[0]);// planes[0] = magnitude
    Mat magI = planes[0];

    magI += Scalar::all(1);                    // switch to logarithmic scale
    log(magI, magI);

    // crop the spectrum, if it has an odd number of rows or columns
    magI = magI(Rect(0, 0, magI.cols & -2, magI.rows & -2));

    // rearrange the quadrants of Fourier image  so that the origin is at the image center
    int cx = magI.cols/2;
    int cy = magI.rows/2;

    Mat q0(magI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
    Mat q1(magI, Rect(cx, 0, cx, cy));  // Top-Right
    Mat q2(magI, Rect(0, cy, cx, cy));  // Bottom-Left
    Mat q3(magI, Rect(cx, cy, cx, cy)); // Bottom-Right

    Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
    q0.copyTo(tmp);
    q3.copyTo(q0);
    tmp.copyTo(q3);

    q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
    q2.copyTo(q1);
    tmp.copyTo(q2);

    return magI;
}





Mat HighPass2D (Mat image)
{
	float dHeight = 1/(image.rows - 1.0);
	float dWidth = 1/(image.cols - 1.0);
	cv::Mat Eta = cv::Mat::zeros(image.rows, 1, CV_32FC1);
	cv::Mat Neta = cv::Mat::zeros(1, image.cols, CV_32FC1);

	for (int i = 0; i < image.rows; i++)
	{
		Eta.row(i) = cos(M_PI * (-0.5 + i*dHeight));
	}

	for (int i = 0; i < image.cols; i++)
	{
		Neta.col(i) = cos(M_PI * (-0.5 + i*dWidth));
	}

	cv::Mat HighPassFilter = Eta*Neta; 
	HighPassFilter = (1.0 - HighPassFilter);
	HighPassFilter = HighPassFilter.mul(2.0 - HighPassFilter);	
	image = HighPassFilter.mul(image);

	return image;
}





Mat LogPolar (Mat image)
{
	//double base = pow(10.0, ((log10((double)image.rows)) / ((double)image.rows))); //radius of the blind spot
	//int ro0 = (int) pow(base, (double) image.rows);
	//int ro0 = 30;
    //int R = image.cols;  //number of rings
    //Point2i center(image.cols/2, image.rows/2);
	//LogPolar_Interp bilin(image.cols, image.rows, center, R, ro0);
	//cv::Mat LogPolar = bilin.to_cortical(image);




	image.convertTo(image, CV_32FC1);
	cv::Mat LogPolar;
	Mat map_x, map_y;
	LogPolar.create( image.size(), image.type() );
	map_x.create( image.size(), CV_32FC1 );
	map_y.create( image.size(), CV_32FC1 );


	double Xc = image.cols/2;
	double Yc = image.rows/2;

	double radius, Theta, x, y;

	double dTheta = (2.0*M_PI)/image.cols;
	double base = pow(10.0, ((log10((double)image.rows))/image.rows));

	for (int i = 0; i < image.rows; i++)
	{
		for (int j = 0; j < image.cols; j++)
		{
			radius = pow(base, i - 1);
			Theta = j * dTheta;

			x = round(radius * cos(Theta) + Xc);
			y = round(radius * sin(Theta) + Yc);
			
			if (x > 0 && y > 0)
			{
				map_x.at<float>(i,j) = x ;
				map_y.at<float>(i,j) = y ;
			}
		}		
	}

	remap(image, LogPolar, map_x, map_y, CV_INTER_LINEAR, BORDER_CONSTANT, Scalar(0, 0, 0));
	return LogPolar;
}