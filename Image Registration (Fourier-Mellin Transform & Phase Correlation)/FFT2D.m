%   Developer:      Gajendren Kasithasan
%   Supervisors:    Dr. D. Armitage & Professor A. Peyton
%   The University of Manchester





% % function f = FFT2D(image)
%FFT2D performs a two-dimensional Discrete Fourier Transform on an image
%through the Fast Fourier Transform algorithm.

%   F = FFT2D(IMAGE) computes the Discrete Fourier Transform of any input
%   image through the use of the Fast Fourier Transform algorithm. 

clear;
tic;

%%                           -- TEST IMAGE 1 -- 

% Read image
image = rgb2gray(imread('Lion Statue.jpg'));

% Display input
figure; imshow(image); title(sprintf(['INPUT IMAGE\nSIZE ' ...
    num2str(size(image))]));





%%                           -- IMAGE SIZE -- 

% Calculate the number of rows and columns in the image
[rows, columns] = size(image);





%%                      -- FAST FOURIER TRANSFORM --

